*italic text*

**bold text**

***bold and italic***

* list item 01
* list item 02
  * sub item here
* list item 03

two spaces  
a new line

`code`

``code includes ` character``

### multiple code blocks
    code line 1
    code line 2
    code line 3

[Bing](http://www.bing.com/)

# h1
## h2
### h3
#### h4
##### h5
###### h6

> quotes here (moved leftward)
 
<http://www.bing.com/>

![](http://www.w3schools.com/images/compatible_firefox.gif)

sent email to <dailyefforts@gmail.com>

HTML
---------

<table>
  <tr>
    <td>00</td>
    <td>01</td>
  </tr>
  <tr>
    <td>10</td>
    <td>11</td>
  </tr>
</table>

<img src="http://www.w3schools.com/images/compatible_firefox.gif" alt="Firefox" width="42" height="42">  

<a href="https://www.mozilla.org/en-US/firefox/">Firefox</a>

~~deleted~~

&copy;
