Use case图用来描述App所能提供给用户的功能。
一个Use case图包含4个主要的元素：
* actors - 使用此App的用户
* system - App
* use cases - App所能提供给用户的一项功能, 一个use case就是用户和App的一次交互 
