/*
 * main.h
 *
 *  Created on: Nov 17, 2012
 *      Author: DailyEfforts
 */

#ifndef MAIN_H_
#define MAIN_H_

#include "array/arr.h"
#include "basic/basic.h"
#include "cpu/cpu_info.h"
#include "mem/mem.h"
#include "pointer/pointer.h"
#include "main/main_arg.h"
#include "struct/struct.h"
#include "macro/macro.h"

#endif /* MAIN_H_ */
